<?php
include '../jaxl.php';

$test = new JAXL(array(
    'jid' => 'chirag',
	'pass' => 'admin',

	// (required)
	'bosh_url' => 'http://localhost:7070/http-bind/',

	// (optional) srv lookup is done if not provided
	// for bosh client 'host' value is used for 'route' attribute
	'host' => 'localhost',

	// (optional) result from srv lookup used by default
	// for bosh client 'port' value is used for 'route' attribute
	'port' => 5222,

	// (optional)
	//'resource' => 'resource',
	
	// (optional) defaults to PLAIN if supported, else other methods will be automatically tried
	'auth_type' => 'PLAIN',
	
	'log_level' => JAXL_INFO
));
$client->add_cb('on_auth_success', function() {
	global $client;
        echo $client->full_jid->to_string();
	_info("got on_auth_success cb, jid ".$client->full_jid->to_string());
	$client->set_status("available!", "dnd", 10);
},1);

$client->add_cb('on_auth_failure', function($reason) {
	global $client;
	$client->send_end_stream();
	_info("got on_auth_failure cb with reason $reason");
},1);
$client->start();
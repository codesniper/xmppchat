<?php

header('content-type', 'plain/text');
include './includes/db.php';

include './includes/XMPPHP/XMPP.php';
//Get message from post
$message = filter_input(INPUT_POST, 'message');
//Get receiver name
$receiver = filter_input(INPUT_POST, 'to');

//modify receiver as per openfire server
$to = strtolower($receiver) . '@' . $config['openfire']['host'];
//Set sender
$sender = $_SESSION['chatId'];
$from = $_SESSION['chatId'] . '@' . $config['openfire']['host'];
// Store this message in our server
$query = "INSERT INTO user_chat_conversation (`from`,`to`,`message`) VALUES ('" . $sender . "','" . $receiver . "','" . $message . "')";
$connection->query($query);
// Destroy older query
unset($query);

$userid = $_SESSION['userid'];
//Get sender details
$query = "SELECT * FROM users WHERE `id` = '$userid'";
$result = $connection->query($query);
while ($row = mysqli_fetch_array($result, 1)) {
    $SenderChatPassword = $row['plain_password'];
    $SenderChatId = $row['chat_jid'];
}
$_SESSION['openChatBoxes'][$receiver] = date('Y-m-d H:i:s', time());
//Save this message in session
if (!isset($_SESSION['chatHistory'][$receiver])) {
    $_SESSION['chatHistory'][$receiver] = '';
}
$_SESSION['chatHistory'][$receiver] .= <<<EOD
					   {
			"s": "1",
			"f": "{$receiver}",
			"m": "{$message}"
	   },
EOD;

$conn = new XMPPHP_XMPP('52.37.161.6', 5222, "$SenderChatId", "$SenderChatPassword", NULL, 'xmpphp', $printlog = true, $loglevel = XMPPHP_Log::LEVEL_VERBOSE);
$conn->autoSubscribe();
try {
    $conn->useEncryption(false);
    $conn->connect();
    $conn->processUntil("session_start", 10);
    $conn->message($to, $message, 'chat');
    $conn->disconnect();
} catch (XMPPHP_Exception $ex) {
    echo $ex->getMessage() . ' in ' . $ex->getFile() . ' on line ' . $ex->getLine();
}
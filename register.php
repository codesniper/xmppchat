<?php
require_once './includes/header.php';
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    //filter and validate username
    $username = filter_input(INPUT_POST, 'username');
    //Filter and validate password
    $password = filter_input(INPUT_POST, 'password');
    //Get email from POST
    $emailFromPost = filter_input(INPUT_POST, 'email');
    //Filter Input
    $email = filter_var($emailFromPost, FILTER_VALIDATE_EMAIL);
    //Escape Email field
    
    //Get name from POST
    $name = filter_input(INPUT_POST, 'name');
    if ($username == FALSE) {
        die('Username is not valid');
    } elseif ($password == FALSE) {
        die('Password is not valid');
    } elseif ($email == FALSE) {
        die('Email is not valid');
    } elseif ($name == FALSE) {
        die('Name is not valid');
    }

    try {
        //Register user in openfire
        $data = array(
            'username' => $username,
            'password' => $password,
            'name' => $name,
            'email' => $email
        );
        //Convert the array to Json
        $arrayToJson = json_encode($data);
        //Open Fire url
        $ofUrl = "http://localhost:9090/plugins/restapi/v1/users";
        //Init cUrl
        $ch = curl_init($ofUrl);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        //Turn of server and pakagemanager
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        //Set header
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $arrayToJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: ypRrps4J58Uy6vMt', 'Accept : application/json', 'Content-Type: application/json', 'Content-Length: ' . strlen($arrayToJson)));
        //Save if success
        $result = curl_exec($ch);
        if (empty($result)) {
            echo "<pre>";
            print_r($result);
            /**
             * @todo We can create exception system here, hint: UserAlreadyExistsException
             */
        }
        $chatJid = $username;
        //Query to store password
        $query = "INSERT INTO users (`username`,`password`,`plain_password`,`email`,`name`,`chat_id`,`chat_jid`) VALUES ('" . $username . "','" . md5($password) . "','" . $password . "','" . $email . "','" . $name . "','" . $email . "','" . $chatJid . "');";
        //Save data to database
        $connection->query($query);
        header("Location:login.php");
    } catch (Exception $ex) {
        die($ex->getCode() . ' : ' . $ex->getMessage() . ' in file ' . $ex->getFile() . ' on line ' . $ex->getLine());
    }
}
?>
<div class="container">
    <div class="login-box">
        <div>
            <div class="login-form row">
                <div class="col-sm-12 text-center login-header">
                    <i class="login-logo fa fa-connectdevelop fa-5x"></i>
                    <h4 class="login-title">Chat Registartion</h4>
                </div>
                <div class="col-sm-12">
                    <div class="login-body">
                        <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                            <div class="control">
                                <input type="text" class="form-control" placeholder="username" name="username" />
                            </div>
                            <div class="control">
                                <input type="email" class="form-control" placeholder="Email" name="email" />
                            </div>
                            <div class="control">
                                <input type="name" class="form-control" placeholder="name" name="name" />
                            </div>
                            <div class="control">
                                <input type="password" class="form-control" placeholder="password" name="password" />
                            </div>
                            <div class="login-button text-center">
                                <input type="submit" class="btn btn-primary" value="Register">
                            </div>
                        </form>

                        <div class="login-footer">
                            <span class="text-right"><a href="#" class="color-white">Forgot password?</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once './includes/footer.php'; ?>
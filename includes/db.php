<?php

ob_start();
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
$config = parse_ini_file('./config/config.ini', 'true');
$connection = new mysqli($config['database']['host'], $config['database']['user'], $config['database']['password'], $config['database']['database']);
function escapeString($string) {
    $result = mysqli_real_escape_string($string);
    if ($result == FALSE) {
        return $string;
    } else {
        return FALSE;
    }
}

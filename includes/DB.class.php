<?php

class DB {

    protected static $connection;

    /**
     * Connect to database
     * 
     * @return boolean false on failure / mysqli MySQLi object instance on success
     */
    public function connect() {
        //Try and connect to database
        if (!isset(self::$connection)) {
            //Load Configuration file
            $config = parse_ini_file('../config/database.ini');

            //Make connection to database
            self::$connection = new mysqli($config['db_host'], $config['db_user'], $config['db_pass'], $config['db_name']);
        }
        //IF the connection was not successfull, handle the error
        if (self::$connection == FALSE) {
            /**
             * @todo Log the error and show excetion to user too
             */
            return FALSE;
        }return self::$connection;
    }

    /**
     * Execute the query
     * 
     * @param $query The query string
     * @return mixed The result of the mysqli::query() function
     */
    public function query($query = "") {
        //Connect to database
        $connect = $this->connect();
        //Execute the query
        $result = $connect->query($query);
        return $result;
    }

    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param $query The query string
     * @return bool False on failure / array Database rows on success
     */
    public static function find($query) {
        $rows = array();
        $result = $this->query($query);

        if ($result === FALSE) {
            return FALSE;
        }

        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Fetch the last error from the database
     * 
     * @return string Database error message
     */
    public function error() {
        $connection = $this->connect();
        return $connection->error;
    }

    /**
     * Quote and escape value for use in a database query
     *
     * @param string $value The value to be quoted and escaped
     * @return string The quoted and escaped string
     */
    public function quote($value) {
        $connection = $this->connect();
        return "'" . $connection->real_escape_string($value) . "'";
    }

    public static function save($data = array()) {
        $saveFields = "";
        $saveValue = "";
        foreach ($data as $model => $saveArray) {
            foreach ($saveArray as $key => $value) {
                $saveFields = implode('`, `', array_keys($saveArray));
                $saveValue = implode('", "', array_values($saveArray));
            }
        }
        $query = 'INSERT INTO `' . $model . '`(`' . $saveFields . '`) VALUES ("' . $saveValue . '");';
        try {
            if ($this->query($query)) {
                return TRUE;
                echo "1";
            } else {
                echo "2";
                return FALSE;
            }
            exit;
        } catch (Exception $ex) {
            echo $ex->getCode() . ' : ' . $ex->getMessage() . ' on Line ' . $ex->getLine() . ' in file ' . $ex->getFile();
            die();
        }
    }

}
